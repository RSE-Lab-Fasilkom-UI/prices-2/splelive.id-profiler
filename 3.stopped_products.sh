# cache top result to get a speed up
ps -aux > .temp

# get all running product name
products=$(cat .temp | grep --color /bin/bash\ /var/www/product[s] | awk -F '/var/www/products/' '{print $2}' | cut -d'/' -f 1 | sort | uniq)
products=$(echo $products | sed 's/ /|/g')"|nginx|node|temp"

# exclude all file with running
ls /var/www/products | grep -Ev $products
