# cache top result to get a speed up
ps aux > .temp
#top -b -n 1 > .temp

# get all product name
products=$(cat .temp| grep --color /bin/bash\ /var/www/product[s] | awk -F '/var/www/products/' '{print $2}' | cut -d'/' -f 1 | sort | uniq)

# echo '<PID> <USER> <RES> <%CPU> <%MEM> <TIME> <CMD>'
# get the detail of each product
for ii in $products; do
  echo $ii | grep --color $ii
  cat /var/www/products/nginx/"$ii".conf | grep server_name | tail -n 1| xargs
  echo
done
rm .temp
