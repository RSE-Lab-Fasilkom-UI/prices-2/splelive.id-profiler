#!/bin/bash
# cache top result to get a speed up
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ps -aux > .temp

# get all product name
products=$(cat .temp | grep --color /bin/bash\ /var/www/product[s] | awk -F '/var/www/products/' '{print $2}' | cut -d'/' -f 1 | sort | uniq)

# echo '<PID> <USER> <RES> <%CPU> <%MEM> <TIME> <CMD>'
# get the detail of each product
for ii in $products; do
  echo $ii | grep --color $ii
  $DIR/1.2.process_detail.sh "$ii"
done
rm .temp
