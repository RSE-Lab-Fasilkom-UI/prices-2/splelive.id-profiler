while read productname; do
  echo "stopping "$productname" ..."
  /var/www/engine/cli/runner.py -cfg /var/www/engine/cli/config.ini destroy "$productname";
done < "./product_to_stop.list"
