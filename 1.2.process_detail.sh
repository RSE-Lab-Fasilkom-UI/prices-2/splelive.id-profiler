if [ "$#" -ne 1 ]; then
    echo "usage: ./process_detail.sh <ProductName>"
    echo " to know product name please use script './running_products.sh'"
    exit 0;
fi
top -b -n 1| grep -i --color "$@"
cat /var/www/products/$1/$1.json | grep "address" | xargs
