if [ "$#" -eq 1 ]; then
    batch=.$1
else
    echo 'first parameter could be a number that tell which access.log is used, for instance,if first parameter is "3" then it is access.log.3'
fi
cat /var/log/nginx/access.log | head -n 1 | awk '{print "First: "$4"]"}'
cat /var/log/nginx/access.log | tail -n 1 | awk '{print "Last: "$4"]"}'
cat /var/log/nginx/access.log$batch | awk '{print $1}' | sort | uniq -c