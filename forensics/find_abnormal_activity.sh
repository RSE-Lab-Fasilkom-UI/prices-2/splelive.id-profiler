if [ "$#" -eq 1 ]; then
    batch=.$1
else
    echo 'first parameter could be a number that tell which access.log is used, for instance, if first parameter is "3" then it is access.log.3'
fi
cat /var/log/nginx/access.log$batch | grep -Ev "152.118|127.0.0.1|.html|.js|.png|.jpg|.css|.ico|.jpeg" |  awk '{print "source: "$1"---\n  Request: "$6"---"$7"\n  Return Code: "$9"\n  Date: "$4"]"}' 