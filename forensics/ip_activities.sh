if [ "$#" -eq 0 ]; then
    echo 'usage "[batch=[1..N],optional] [filter=[1,0],optional] ./ip_activities.sh <ip_address>"'
    echo 'example "batch=1 filter=1 ./ip_activities.sh 152.118.192.20"'
    exit
fi
#elif [ "$#" -eq 2 ]; then
#    ip_addr="$1"
#    batch=".$2"
#else
#    ip_addr="$1"
#fi

#while getopts ":b:f:" opt; do
#  case $opt in
#    a) batch="$OPTARG"
#    ;;
#    p) filter="$OPTARG"
#    ;;
#    \?) echo "Invalid option -$OPTARG" >&2
#    ;;
#  esac
#done

ip_addr="$1"
# echo "batch $batch"
# echo "filter $filter"

if [ -z $filter ]; then
  filter=0
fi
if [ ! -z $batch ]; then
  batch=.$batch
fi

if [ $filter -eq 1 ]; then
  cat /var/log/nginx/access.log$batch | grep $ip_addr  | grep -Ev ".html|.js|.png|.jpg|.css|.ico|.jpeg" |  awk '{print "source: "$1"---\n  Request: "$6"---"$7"\n  Date: "$4"] \n  Return Code: "$9}' 
else
  cat /var/log/nginx/access.log$batch | grep $ip_addr  |  awk '{print "source: "$1"---\n  Request: "$6"---"$7"\n  Date: "$4"] \n  Return Code: "$9}' 
fi
