while read productname; do
  echo "starting "$productname" ..."
  /var/www/engine/cli/runner.py -cfg /var/www/engine/cli/config.ini restart "$productname";
  sleep 15s
done < <(ls /var/www/products)
